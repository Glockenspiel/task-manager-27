package ru.t1.sukhorukova.tm.exception.field;

public final class StatusEmptyException extends AbstractFieldExceprion {

    public StatusEmptyException() {
        super("Error! Status is empty...");
    }

}
