package ru.t1.sukhorukova.tm.command.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.command.AbstractCommand;
import ru.t1.sukhorukova.tm.dto.Domain;
import ru.t1.sukhorukova.tm.enumerated.Role;

@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull public static final String FILE_BASE64 = "./data.base64";
    @NotNull public static final String FILE_BINARY = "./data.bin";

    @Nullable @Override
    public String getArgument() {
        return null;
    }

    @NotNull @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(locatorService.getProjectService().findAll());
        domain.setTasks(locatorService.getTaskService().findAll());
        domain.setUsers(locatorService.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        locatorService.getProjectService().set(domain.getProjects());
        locatorService.getTaskService().set(domain.getTasks());
        locatorService.getUserService().set(domain.getUsers());
        locatorService.getAuthService().logout();
    }

}
