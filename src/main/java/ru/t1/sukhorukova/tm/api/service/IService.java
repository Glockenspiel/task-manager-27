package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    M add(@Nullable M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    @NotNull
    M removeOne(@Nullable M model);

    @Nullable
    M removeOneById(@Nullable String id);

    @Nullable
    M removeOneByIndex(@Nullable Integer index);

    void removeAll();

    int getSize();

    boolean existsById(String id);

}
